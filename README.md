# API-QR-CARS

# Installation
Pour installer le projet, cloner le projet avec la commande :

    git clone https://gitlab.com/Zgreg/superimmo3000.git

à l'aide de l'invite de commande (terminal) dans le répertoire souhaité.

## Saisir les commandes : 

Changement de répertoire :

    cd api-qr-cars

Installation des dépendances :

    npm install

## injection des fixtures
Rendez-vous sur postman ou sur un navigateur internet, et executez en methode GET l'url suivant :
    http://localhost:4000/api/fixtures/addMultipleVehicle

# Démarrage_du_serveur
Executer la commande :

    npm start
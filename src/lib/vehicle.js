// const {
//     ReasonPhrases,
//     StatusCodes,
//     getReasonPhrase,
//     getStatusCode,
// } = require('http-status-codes');

// const { vehicle } = require("../api/vehicle");
const { Vehicle } = require("../db/models/vehicle");
const { Brand } = require("../db/models/brand");

async function addVehicle({ image, name, registration, description, mileage, place, energy, brand }) {
    return await Vehicle.create({ image, name, registration, description, mileage, place, energy, brand });
}

/**
 * Get all Vehicles
 */
async function getAllVehicles() {
    return await Vehicle.find()//.populate("brand").select()
}

/**
 * Get one Vehicle by ID
 * 
 * @param { registration } registration 
 * @returns Vehicle
 */
async function getOneVehicle({ registration : registration }) {
    return await Vehicle.findOne({ registration })
}


/**
 * 
 * @param {Vehicle} vehicle 
 * @param {*} param1 
 * @returns 
 */
async function updateVehicle(vehicle, { image, name, registration, description, mileage, place, energy, brand }) {
        vehicle.image =  image;
        vehicle.name =  name;
        vehicle.registration =  registration;
        vehicle.description =  description;
        vehicle.mileage =  mileage;
        vehicle.place =  place;
        vehicle.energy =  energy;
        vehicle.brand =  brand;

        return await vehicle.save();
    // }
}

/**
 * Delete one vehicle by ID
 */
async function deleteVehicle( vehicle ) {
    return await vehicle.remove();
}


module.exports = { 
    addVehicle,
    getAllVehicles,
    getOneVehicle,
    updateVehicle,
    deleteVehicle
}
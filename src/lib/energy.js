const { Energy } = require("../db/models/energy");

async function addEnergy({ name }) {
    return await Energy.create({ name });
}

/**
 * Get all Energies
 */
async function getAllEnergies() {
    return await Energy.find()
}

/**
 * Get one Energy by ID
 * 
 * @param { _id } id 
 * @returns Energy
 */
async function getOneEnergy({ id : _id }) {
    return await Energy.findOne({ _id })
}


/**
 * 
 * @param {Energy} Energy 
 * @param {*} param1 
 * @returns 
 */
async function updateEnergy(Energy, { name }) {
        Energy.name =  name;

        return await Energy.save();
}

/**
 * Delete one Energy by ID
 */
async function deleteEnergy( Energy ) {
    return await Energy.remove();
}


module.exports = { 
    addEnergy,
    getAllEnergies,
    getOneEnergy,
    updateEnergy,
    deleteEnergy
}
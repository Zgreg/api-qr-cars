const { Brand } = require("../db/models/brand");

async function addBrand({ name }) {
    return await Brand.create({ name });
}

/**
 * Get all Brands
 */
async function getAllBrands() {
    return await Brand.find()
}

/**
 * Get one Brand by ID
 * 
 * @param { _id } id 
 * @returns Brand
 */
async function getOneBrand({ id : _id }) {
    return await Brand.findOne({ _id })
}


/**
 * 
 * @param {Brand} Brand 
 * @param {*} param1 
 * @returns 
 */
async function updateBrand(brand, { name }) {
        // console.log(brand);
        brand.name =  name;

        return await brand.save();
}

/**
 * Delete one Brand by ID
 */
async function deleteBrand( brand ) {
    return await brand.remove();
}


module.exports = { 
    addBrand,
    getAllBrands,
    getOneBrand,
    updateBrand,
    deleteBrand
}
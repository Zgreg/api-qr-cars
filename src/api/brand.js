const {
    ReasonPhrases,
    StatusCodes,
} = require('http-status-codes');

const express = require("express");
const bp = require('body-parser');
const api = express();
const _ = require("lodash");

const {
    addBrand,
    getAllBrands,
    getOneBrand,
    updateBrand,
    deleteBrand
} = require('../lib/brand');

api.use(bp.json());

api.post('/brand', async (req, res) => {
    const { name } = req.body;
    let brand;

    try {
        brand = await addBrand({ name })
    } catch (err) {
        res.status(StatusCodes.BAD_REQUEST).json(err);
    }

    res.status(StatusCodes.CREATED).json(brand);
});

api.get('/brand', async (req, res) => {
    let brand;

    try {
        brand = await getAllBrands();
    } catch (err) {
        res.status(StatusCodes.BAD_REQUEST).json(err);
    }

    res.status(StatusCodes.OK).json(brand);
})

api.get('/brand/:id', async (req, res) => {
    const { id: _id } = req.params;

    let brand;

    try {
        brand = await getOneBrand({ id : _id });
    } catch (err) {
        const type = typeof err;
        const error = { err, type }
        res.status(StatusCodes.BAD_REQUEST).json(error);
    }

    if (brand) {
        res.status(StatusCodes.OK).json(brand);
    } else {
        res.status(StatusCodes.NOT_FOUND).json({
            message : ReasonPhrases.NOT_FOUND
        });
    }
})

api.put('/brand/:id', async (req, res) => {
    const { name } = req.body;
    const { id: _id } = req.params;

    let  brand;

    try {
        brand = await getOneBrand({ id : _id });
    } catch (err) {
        const type = typeof err;
        const error = { err, type }
        console.log("la ?")
        res.status(StatusCodes.BAD_REQUEST).json(error);
    }

    if ( brand ) {
        try {
            brandUpdated = await updateBrand(brand , { name })
        } catch (err) {
            res.status(StatusCodes.BAD_REQUEST).json(err);
        }

        res.status(StatusCodes.OK).json(brandUpdated);

    } else {
        res.status(StatusCodes.NOT_FOUND).json({
            message : ReasonPhrases.NOT_FOUND
        })
    }
    
})

api.delete('/brand/:id', async (req, res) => {
    const { id : _id } = req.params;

    let  brand;
    try {
        brand = await getOneBrand({ id : _id });
    } catch (err) {
        const type = typeof err;
        const error = { err, type }
        res.status(StatusCodes.BAD_REQUEST).json(error);
    }

    if (brand) {
        try {
            brandDeleted = deleteBrand(brand)
        } catch (err) {
            res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(err);
        }

        res.status(StatusCodes.NO_CONTENT).end();
    } else {
        res.status(StatusCodes.NOT_FOUND).json({
            message : ReasonPhrases.NOT_FOUND
        });
    }
})

brand = api;

module.exports = { brand }
const {
    ReasonPhrases,
    StatusCodes,
} = require('http-status-codes');

const express = require("express");
const bp = require('body-parser');
const api = express();
const _ = require("lodash");
const mongoose = require('mongoose');

const { addVehicle, getAllVehicles } = require('../lib/vehicle');
const { addBrand, getAllBrands } = require('../lib/brand');
const { addEnergy, getAllEnergies } = require('../lib/energy');

// const { Brand } = require("../db/schema/BrandSchema")
// const { Brand } = require("../db/models/brand");
// const { Energy } = require("../db/models/energy");
const { Vehicle } = require("../db/models/vehicle");
// const { energy } = require('./energy');
const { result } = require('lodash');

api.use(bp.json());

api.get('/fixtures/addMultipleVehicle', async (req, res) => {
    mongoose.connection.db.dropDatabase();

    try {
        Vehicle.create(
            //-- Tesla Zone
            {
                image : "https://www.automobile-magazine.fr/asset/cms/840x394/176304/config/125048/model-s.jpg",
                name : "Modèle S",
                registration : "TS-001-EM",
                description : "La Tesla Model S est une voiture électrique haut de gamme et 100 % électrique produit par le constructeur automobile américain Tesla.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Électrique",
                brand : "Tesla"
            },
            {
                image : "https://www.automobile-propre.com/wp-content/uploads/2020/10/Tesla-Model-3-Performance-2020.jpg",
                name : "Modèle 3",
                registration : "TS-002-EM",
                description : "La Tesla Model 3 est une berline familiale haut de gamme et 100 % électrique, produit par le constructeur automobile américain Tesla",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Électrique",
                brand : "Tesla"
            },
            {
                image : "https://tesla-cdn.thron.com/delivery/public/image/tesla/3863f3e5-546a-4b22-bcbc-1f8ee0479744/bvlatuR/std/1200x628/MX-Social",
                name : "Modèle X",
                registration : "TS-003-EM",
                description : "Le Model X est un SUV familial haut de gamme et 100 % électrique produit par le constructeur automobile américain Tesla.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Électrique",
                brand : "Tesla"
            },
            {
                image : "https://www.turbo.fr/sites/default/files/styles/article_690x405/public/2019-04/tesla-model-y-suv-7-places.png?itok=Qa2qutp-",
                name : "Modèle Y",
                registration : "TS-004-EM",
                description : "Tesla Model Y est un SUV compact haut de gamme 100 % électrique, produit par le constructeur automobile américain Tesla.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Électrique",
                brand : "Tesla"
            },
            //-- Ford Zone
            {
                image : "https://feassets.bymycar.fr/vn/47/31950/0/ford-puma-10-ecoboost-125-ch-mhev-s-s-bvm6-neuf-2021-nice.jpg",
                name : "Puma",
                registration : "FD-001-FD",
                description : "La Puma est un crossover urbain commercialisé par le constructeur automobile américain Ford.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Diesel",
                brand : "Ford"
            },
            {
                image : "https://imgservprod.bymycar.fr/image/?apikey=437fe90bfc2e7879f0953434890aa246&url=https://feassets.bymycar.fr/vn/31/33034/0/ford-kuga-25-duratec-190-ch-fhev-e-cvt-s-s-neuf-2021-venissieux.jpg&dpr=2&w=402&h=292&t=square&a=center&q=50&output=jpg&bg=ffffff",
                name : "Kuga",
                registration : "FD-002-FD",
                description : "Le Ford Kuga est un SUV produit par le constructeur automobile américain Ford.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Essence",
                brand : "Ford"
            },
            {
                image : "https://www.gpas-cache.ford.com/guid/9dd7a18e-b616-3765-be50-71423c60e458.png",
                name : "Mustang mach-e",
                registration : "FD-003-FD",
                description : "La Mustang Mach-E est un SUV coupé compact électrique du constructeur automobile américain Ford.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Électrique",
                brand : "Ford"
            },
            {
                image : "https://www.gpas-cache.ford.com/guid/47601eed-a5b2-3507-8643-e9327679a956.png",
                name : "Fiesta",
                registration : "FD-004-FD",
                description : "La Ford Fiesta est un modèle d'automobile 3-5 portes produit par le constructeur américain Ford.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Essence",
                brand : "Ford"
            },

            //-- Peugeot Zone
            {
                image : "https://www.largus.fr/images/images/peugeot-108-gt-line.jpg",
                name : "108",
                registration : "PG-001-PG",
                description : "La Peugeot 108, est une citadine du constructeur automobile français Peugeot.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Diesel",
                brand : "Peugeot"
            },
            {
                image : "https://visuel3d-secure.peugeot.com/V3DImage.ashx?client=CFGAP3D&ratio=1&format=jpg&quality=90&version=1PP2A5FMZJB0A0C3&color=0MM00N2T&trim=0PA40RFX&width=640&view=001",
                name : "208",
                registration : "PG-002-PG",
                description : "La 208 est une voiture de segment B produite par le constructeur automobile français Peugeot.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Électrique",
                brand : "Peugeot"
            },
            {
                image : "https://visuel3d-secure.peugeot.com/V3DImage.ashx?client=CFGAP3D&ratio=1&format=jpg&quality=90&version=1PP1SYHMTKB0A0C5&color=0MM00N2S&trim=0PA50RFX&width=640&view=002",
                name : "2008",
                registration : "PG-003-PG",
                description : "La 2008 II est un crossover urbain commercialisé par le constructeur automobile français Peugeot.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Électrique",
                brand : "Peugeot"
            },
            {
                image : "https://cdn.drivek.it/configurator-covermobile/cars/fr/$original$/PEUGEOT/308/31386_BERLINE-A-HAYON-5-PORTES/peugeot-308-2017-cover-mobile.jpg",
                name : "308",
                registration : "PG-004-PG",
                description : "La 308 est une automobile compacte du constructeur français Peugeot.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Électrique",
                brand : "Peugeot"
            },

            //-- Renault Zone
            {
                image : "https://offres.renault.be/img/cars/versions/twizy-LIFE_45_B-Buy.png",
                name : "Twizy e-tech",
                registration : "RT-001-RT",
                description : "Renault Twizy est une petite voiture électrique (écomobilité), de la série Renault Z.E.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Électrique",
                brand : "Renault"
            },
            {
                image : "https://cdn.drivek.it/configurator-imgs/trucks/fr/1600/RENAULT/KANGOO-EXPRESS/5722_FOURGONNETTE-4-PORTES/renault-kangoo-ze-van-4-doors.jpg",
                name : "Kangoo 3 Express",
                registration : "RT-002-RT",
                description : "La Kangoo III est un ludospace ou utilitaire produit par le constructeur automobile français Renault.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Essence",
                brand : "Renault"
            },
            {
                image : "https://blogzineauto.com/wp-content/uploads/2019/03/Renault-clio-5-orange-valencia-1.jpg",
                name : "Clio 5",
                registration : "RT-003-RT",
                description : "La Clio 5 est une automobile citadine du constructeur automobile français Renault.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Diesel",
                brand : "Renault"
            },
            {
                image : "https://www.largus.fr/images/images/renault-captur-2-2019-1.jpg?width=612&quality=80",
                name : "Captur 2",
                registration : "RT-004-RT",
                description : "La Captur II (code interne XJB) est un crossover du constructeur automobile français Renault.",
                mileage : Math.floor(Math.random() * 100000) + 5000,
                place : 5,
                energy : "Diesel",
                brand : "Renault"
            },
        )
    } catch (err) {
        res.status(StatusCodes.BAD_REQUEST).json(err);
    }

    // res.status(StatusCodes.OK).end();
    res.status(StatusCodes.OK).json({message : "Véhicules enregistrés"});
})

fixtures = api;

module.exports = { fixtures }
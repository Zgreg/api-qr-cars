const {
    ReasonPhrases,
    StatusCodes,
} = require('http-status-codes');

const express = require("express");
const bp = require('body-parser');
const api = express();
const _ = require("lodash");

const {
    addEnergy,
    getAllEnergies,
    getOneEnergy,
    updateEnergy,
    deleteEnergy
} = require('../lib/energy');

api.use(bp.json());

api.post('/energy', async (req, res) => {
    const { name } = req.body;
    let energy;

    try {
        energy = await addEnergy({ name })
    } catch (err) {
        res.status(StatusCodes.BAD_REQUEST).json(err);
    }

    res.status(StatusCodes.CREATED).json(energy);
});

api.get('/energy', async (req, res) => {
    let energy;

    try {
        energy = await getAllEnergies();
    } catch (err) {
        res.status(StatusCodes.BAD_REQUEST).json(err);
    }

    res.status(StatusCodes.OK).json(energy);
})

api.get('/energy/:id', async (req, res) => {
    const { id: _id } = req.params;

    let energy;

    try {
        energy = await getOneEnergy({ id : _id });
    } catch (err) {
        const type = typeof err;
        const error = { err, type }
        res.status(StatusCodes.BAD_REQUEST).json(error);
    }

    if (energy) {
        res.status(StatusCodes.OK).json(energy);
    } else {
        res.status(StatusCodes.NOT_FOUND).json({
            message : ReasonPhrases.NOT_FOUND
        });
    }
})

api.put('/energy/:id', async (req, res) => {
    const { name } = req.body;
    const { id: _id } = req.params;

    let  energy;

    try {
        energy = await getOneEnergy({ id : _id });
    } catch (err) {
        const type = typeof err;
        const error = { err, type }
        console.log("la ?")
        res.status(StatusCodes.BAD_REQUEST).json(error);
    }

    if ( energy ) {
        try {
            energyUpdated = await updateEnergy(energy , { name })
        } catch (err) {
            res.status(StatusCodes.BAD_REQUEST).json(err);
        }

        res.status(StatusCodes.OK).json(energyUpdated);

    } else {
        res.status(StatusCodes.NOT_FOUND).json({
            message : ReasonPhrases.NOT_FOUND
        })
    }
    
})

api.delete('/energy/:id', async (req, res) => {
    const { id : _id } = req.params;

    let  energy;
    try {
        energy = await getOneEnergy({ id : _id });
    } catch (err) {
        const type = typeof err;
        const error = { err, type }
        res.status(StatusCodes.BAD_REQUEST).json(error);
    }

    if (energy) {
        try {
            energyDeleted = deleteEnergy(energy)
        } catch (err) {
            res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(err);
        }

        res.status(StatusCodes.NO_CONTENT).end();
    } else {
        res.status(StatusCodes.NOT_FOUND).json({
            message : ReasonPhrases.NOT_FOUND
        });
    }
})

energy = api;

module.exports = { energy }
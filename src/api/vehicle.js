const {
    ReasonPhrases,
    StatusCodes,
} = require('http-status-codes');

const express = require("express");
const bp = require('body-parser');
const api = express();
const _ = require("lodash");

const {
    addVehicle,
    getAllVehicles,
    getOneVehicle,
    updateVehicle,
    deleteVehicle,
} = require('../lib/vehicle');

api.use(bp.json());

api.post('/vehicle', async (req, res) => {
    const { image, name, registration, description, mileage, place, energy, brand } = req.body;
    let user;

    try {
        user = await addVehicle({ image, name, registration, description, mileage, place, energy, brand })
    } catch (err) {
        res.status(StatusCodes.BAD_REQUEST).json(err);
    }

    res.status(StatusCodes.OK).json(user);
});

api.get('/vehicle', async (req, res) => {
    let vehicle;

    try {
        vehicle = await getAllVehicles();
    } catch (err) {
        res.status(StatusCodes.BAD_REQUEST).json(err);
    }

    const result = {vehicle : vehicle};

    res.status(StatusCodes.OK).json(result);
})

api.get('/vehicle/:registration', async (req, res) => {
    const { registration: registration } = req.params;

    let vehicle;

    try {
        vehicle = await getOneVehicle({ registration : registration });
    } catch (err) {
        const type = typeof err;
        const error = { err, type }
        res.status(StatusCodes.BAD_REQUEST).json(error);
    }

    if (vehicle) {
        res.status(StatusCodes.OK).json(vehicle);
    } else {
        res.status(StatusCodes.NOT_FOUND).json({
            message : "Service not found"
        });
    }
})

api.put('/vehicle/:id', async (req, res) => {
    const { image, name, registration, description, mileage, place, energy, brand } = req.body;
    const { id: _id } = req.params;

    let  vehicle;

    try {
        vehicle = await getOneVehicle({ id : _id });
    } catch (err) {
        const type = typeof err;
        const error = { err, type }
        res.status(StatusCodes.BAD_REQUEST).json(error);
    }

    if ( vehicle ) {
        try {
            vehicleUpdated = await updateVehicle(vehicle , { image, name, registration, description, mileage, place, energy, brand })
        } catch (err) {
            res.status(StatusCodes.BAD_REQUEST).json(err);
        }

        res.status(StatusCodes.OK).json(vehicleUpdated);

    } else {
        res.status(StatusCodes.NOT_FOUND).json({
            message : "Vehicle not found"
        })
    }
    
})

api.delete('/vehicle/:id', async (req, res) => {
    const { id: _id } = req.params;

    let  vehicle;
    try {
        vehicle = await getOneVehicle({ id : _id });
    } catch (err) {
        const type = typeof err;
        const error = { err, type }
        res.status(StatusCodes.BAD_REQUEST).json(error);
    }

    if (vehicle) {
        try {
            vehicleDeleted = deleteVehicle(vehicle)
        } catch (err) {
            res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(err);
        }

        res.status(StatusCodes.NO_CONTENT).end();
    } else {
        res.status(StatusCodes.NOT_FOUND).json({
            message : "Registry not found"
        });
    }
})

vehicle = api;

module.exports = { vehicle }
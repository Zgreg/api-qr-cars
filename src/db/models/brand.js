const { BrandSchema } = require("../schema/BrandSchema")

const { model } = require("mongoose");

const Brand = model('brand', BrandSchema);

module.exports = { Brand }
const { EnergySchema } = require("../schema/EnergySchema")

const { model } = require("mongoose");

const Energy = model('energy', EnergySchema);

module.exports = { Energy }
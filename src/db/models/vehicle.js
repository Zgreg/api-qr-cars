const { VehicleSchema } = require("../schema/VehicleSchema")

const { model } = require("mongoose");

const Vehicle = model('vehicle', VehicleSchema);

module.exports = { Vehicle }
const mongoose = require('mongoose');

const DB_URI = `mongodb://${process.env.DB_HOST}:27017/${process.env.DB_NAME}`;

function init() {
    mongoose.connect(DB_URI, {
        user: process.env.DB_USER,
        pass: process.env.DB_PASS,
        useUnifiedTopology: true,
        useNewUrlParser: true,
    })
}

module.exports = {
    init
}


const mongoose = require('mongoose');

const { Schema } = mongoose;

const EnergySchema = new Schema({
    name :  {type : String, required: true},
});

module.exports = { EnergySchema }

/** Data exemple :
{
    "name" : "Diesel || Essence || Electrique",
}
 */
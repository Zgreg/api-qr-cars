const mongoose = require('mongoose');

const { Schema } = mongoose;

const BrandSchema = new Schema({
    name :  {type : String, required: true},
});

module.exports = { BrandSchema }

/** Data exemple :
{
    "name" : "Citröen || Tesla || Renault",
}
 */
const mongoose = require('mongoose');

const { Schema } = mongoose;

const VehicleSchema = new Schema({
    image : {type : String, required: true},
    name : {type : String, required: true},
    registration : {type : String, trim: true, required: true},
    description : {type : String, required: true},
    mileage : {type : Number, required : true},
    place : {type : Number, requiered : true},
    energy : {type : String, required: true},
    brand : {type : String, required: true},
    disabled : {type : Boolean, default : false},
});

module.exports = { VehicleSchema }

/** Data exemple :
{
    "image" : "LIEN_IMAGE"
    "name" : "Model 3",
    "registration" : "ZZ-042-ZZ",
    "description" : "Short vehicle description",
    "mileage" : 135000,
    "place" : 5,
    "energy" : "Électrique",
    "brand" : "Tesla"
}
 */
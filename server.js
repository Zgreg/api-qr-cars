require('dotenv').config();

require('./src/db').init();

const express = require("express");

const { vehicle } = require("./src/api/vehicle");
const { brand } = require("./src/api/brand");
const { energy } = require("./src/api/energy");
const { fixtures } = require("./src/api/fixtures");

const app = express();

app.use('/api/', vehicle);
app.use('/api/', brand);
app.use('/api/', energy);
app.use('/api/', fixtures);

app.get("/", (req, res) => {
    res.send("Hello World !").end();
});

app.listen(4000, () => {
    console.log("Ready on port 4000 !");
});